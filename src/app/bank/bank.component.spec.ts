import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import { BankComponent } from './bank.component';
import { Location } from '@angular/common';


describe('BankComponent', () => {
  let component: BankComponent;
  let fixture: ComponentFixture<BankComponent>;
  let router: Router;
  beforeEach(async () => {
     TestBed.configureTestingModule({
      declarations: [ BankComponent ],
      imports: [RouterTestingModule.withRoutes([])]
     })
    .compileComponents();
  });

  beforeEach(() => {
    router = TestBed.inject(Router);
    fixture = TestBed.createComponent(BankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should call onlogout', ()=> {
    const logout = fixture.debugElement.nativeElement.querySelector('a');
    logout.click();
    fixture.detectChanges();
    router = TestBed.get(Router);

    let location:Location = TestBed.get(Location);
    router.navigate(['./logout']).then( ()=>{
      expect(location.path()).toBe('./login');
    });
  });
});
