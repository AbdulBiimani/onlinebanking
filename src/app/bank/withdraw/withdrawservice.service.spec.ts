import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { withdrawservice } from './withdrawservice.service';

describe('WithdrawserviceService', () => {
  let service: withdrawservice;
  let fb : FormBuilder;
  let httpmock: HttpTestingController;

  const dummyWithdrawData ={
  account_number:"132548102",
  debitAmount:"5000",
  description:"grocery"
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, HttpClientTestingModule,FormsModule], 
    });
    httpmock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(withdrawservice);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should call withdrawing()', ()=>{
    service.withdrawing(dummyWithdrawData.account_number, dummyWithdrawData.debitAmount, dummyWithdrawData.description).subscribe((res) =>{

    });
    const req = httpmock.expectOne('http://localhost:9030/api/transaction/withdraw');
    expect(req.request.method).toBe('POST');
    req.flush(dummyWithdrawData);
  });
});
