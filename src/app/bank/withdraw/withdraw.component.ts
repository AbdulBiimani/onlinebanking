import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';
import { withdrawservice } from './withdrawservice.service';
import { NgForm } from '@angular/forms';



// import { DepositserviceService } from '../depositservice.service';


@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
client: Client;  
service : UserService;

  constructor(public withdrawservice: withdrawservice){}
  
  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token'));
  }
  formModel = {
    debitAmount:'',
    description:''
  
};
  onSubmit(form:NgForm){
    let accSelection = <HTMLInputElement>document.getElementById('accountNumber');
    let accNumber = accSelection.value;
   this.withdrawservice.withdrawing(accNumber,this.formModel.debitAmount,this.formModel.description).subscribe(res =>{
     console.log(res)
   });
   this.service.updateToken().subscribe(res =>{
    localStorage.setItem('token', JSON.stringify(res));
    this.client =  JSON.parse(localStorage.getItem('token'));
  });
  } 
}

