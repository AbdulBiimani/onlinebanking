import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';



@Component({
  selector: 'app-fund-transfer',
  templateUrl: './fund-transfer.component.html',
  styles: [
  ]
})

export class FundTransferComponent implements OnInit {
    
    client: Client;
  constructor(private service : UserService,
    ) { 
    

  }

  formModel = {
      amount:''
    
};
  onClick(form:NgForm) {
    let elementsFrom = <HTMLInputElement>document.getElementById('transferFrom');
    let accNumFrom = elementsFrom?.value;
    let elementsTo = <HTMLInputElement>document.getElementById('transferTo');
    let accNumTo = elementsTo?.value;
    let amount1 = this.formModel.amount;
    // console.log(amount1);
    
   //console.log(amount.get('amount')?.value)
   this.service.fundtransfer(accNumFrom, accNumTo, amount1).subscribe((response: any) =>{
     
   });
   this.service.updateToken().subscribe(res =>{
    localStorage.setItem('token', JSON.stringify(res));
    this.client =  JSON.parse(localStorage.getItem('token'));
  });
  }
  user: any
  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token')); //this is how you get the token
    console.log(this.client);
  }

}
