import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';
import { DepositserviceService } from '../depositservice.service';
// import { DepositserviceService } from '../depositservice.service';


@Component({
  selector: 'app-deposit-form',
  templateUrl: './deposit-form.component.html',
  styleUrls: ['./deposit-form.component.css']
})
export class DepositFormComponent implements OnInit{

client:Client;
service: UserService;
  constructor(public depositService: DepositserviceService){}
  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token'));
  }
  formModel = {
    creditAmount:'',
    description:''
  
};
  onSubmit(form:NgForm){
    let accSelection = <HTMLInputElement>document.getElementById('accountNumber');
    let accNumber = accSelection.value;
   this.depositService.depositing(accNumber,this.formModel.creditAmount,this.formModel.description).subscribe(res =>{
     console.log(res)
   });
   this.service.updateToken().subscribe(res =>{
    localStorage.setItem('token', JSON.stringify(res));
    this.client =  JSON.parse(localStorage.getItem('token'));
  });
  } 
}

