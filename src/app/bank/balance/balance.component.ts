import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/user.service';
import { Client } from 'src/app/models/Client';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styles: [
  ]
})
export class BalanceComponent implements OnInit {
  client!:Client;

  constructor(public service: UserService) { }

  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token'));
  }

}
