import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../models/Client';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.css']
})
export class BankComponent implements OnInit {
  client!:Client;

  constructor(private router:Router) { }

  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token'));
  }

  onLogout(){
    localStorage.removeItem('token');
    this.router.navigate(['/user/login']);
  }

}
