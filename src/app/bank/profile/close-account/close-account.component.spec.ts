import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {  ReactiveFormsModule } from '@angular/forms';

import { CloseAccountComponent } from './close-account.component';

describe('CloseAccountComponent', () => {
  let component: CloseAccountComponent;
  let fixture: ComponentFixture<CloseAccountComponent>;
  let httpmock: HttpTestingController;
  let serviceSpy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CloseAccountComponent ],
      imports: [ReactiveFormsModule, HttpClientTestingModule]
     
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CloseAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
