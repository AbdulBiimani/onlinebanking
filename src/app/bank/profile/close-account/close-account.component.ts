import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/Client';
import { UserService } from 'src/app/shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-close-account',
  templateUrl: './close-account.component.html',
  styleUrls: ['./close-account.component.css']
})
export class CloseAccountComponent implements OnInit {
  client!: Client;
  
    
  constructor(private userService:UserService, private toastr: ToastrService, private Router: Router) { }

  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token')); //this is how you get the token
  }

  submitRequest() {
    let accSelection = <HTMLInputElement>document.getElementById('accNum');
    let accNumber = accSelection.value;
//    console.log(accNumber);
    this.userService.deleteAccount(accNumber).subscribe(
      (res: any) => {
        console.log("console printed ")
        if (res.succeded) {
          this.userService.formModel.reset();
          this.toastr.success('New User Submitted', 'Account Close Successfully.');
        } else {
          this.toastr.error("Account cannot close", 'Something went wrong');
        }
      },
      (err: any) => {
        console.log(err);
        if(err.error =="Account is not empty, need to withdraw all the money before closing")
        alert(err.error);
        
      }
    );
    this.userService.updateToken().subscribe(res =>{
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
  }

}
