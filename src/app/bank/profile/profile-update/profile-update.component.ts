import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/models/Client';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-profile-update',
  templateUrl: './profile-update.component.html',
  styleUrls: ['./profile-update.component.css']
})
export class ProfileUpdateComponent implements OnInit {
  client!: Client;

  constructor(public service: UserService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.client = JSON.parse(localStorage.getItem('token'));
    console.log(this.client);
    this.service.formModel.reset();
  }

  onSubmit() {
    this.service.updateProfile().subscribe(
      (res: any) => {
        console.log("console printed " + res)
        if (res.succeded) {
          this.service.formModel.reset();
          this.toastr.success('New User Submitted', 'Registration Successful.');
        } else {
          res.errors.forEach((element: { code: any; description: any }) => {
            switch (element.code) {
              case 'DuplicateUser':
                this.toastr.error("Username is already taken", 'Registration failed');
                break;
              default:
                this.toastr.error(element.description, 'Registration failed');
                break;
            }
          });
        }
      },
      err => {
        console.log(err);
      }
    );
    this.service.updateToken().subscribe(res =>{
      localStorage.setItem('token', JSON.stringify(res));
      this.client =  JSON.parse(localStorage.getItem('token'));
    });
  }

  validateFirstName() {
    return this.service.firstName?.invalid && !this.service.firstName?.untouched;
  }

  validateLastName() {
    return this.service.lastName?.invalid && !this.service.lastName?.untouched;
  }

  validateAddress() {
    return this.service.address?.invalid && !this.service.address?.untouched;
  }



  validateEmail() {
    return this.service.email?.invalid && !this.service.email?.untouched;
  }



  validateButton() {
    return this.service.formModel.invalid;
  }

  validateCheckBox() {
    return this.service.agree?.invalid;
  }

  validateContactNumber() {
    return this.service.contactNumber?.invalid && !this.service.contactNumber?.untouched;
  }

  validateContactNumberExample() {
    return this.service.contactNumber?.invalid;
  }

}
